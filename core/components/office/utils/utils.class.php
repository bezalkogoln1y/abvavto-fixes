<?php

declare(strict_types=1);

class officeUtils
{
    /** @var modX */
    private $modx;

    public function __construct(modX $modx)
    {
        $this->modx = &$modx;
    }

    public function debugLog($msg, string $logFilename = 'office.utils.log')
    {
        $exception = null;

        if (gettype($msg) === 'array') {
            $msg = json_encode($msg, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        }

        if ($msg instanceof Exception) {
            $exception = $msg;
        }

        $options = [
            'target' => 'FILE',
            'options' => [
                'filename' => $logFilename
            ]
        ];

        $this->modx->log(
            xPDO::LOG_LEVEL_ERROR, $exception ? $exception->getMessage() : $msg, $options, '',
            $exception ? $exception->getFile() : '', $exception ? $exception->getLine() : '', 
        );
    }

    public function deepKeyExists($array, $path = null)
    {
        if (!$path) {
            return false;
        }

        if (gettype($path) !== 'array') {
            $path = preg_split('#\.#', $path);
        }

        if (array_key_exists($path[0], $array)) {
            if (count($path) === 1) {
                return true;
            }

            return $this->deepKeyExists($array[$path[0]], array_slice($path, 1));
        }

        return false;
    }
}

return 'officeUtils';
