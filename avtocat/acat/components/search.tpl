<div id="ac-search" class="acSearch" x-data="avtocatSearch">
  <div class="search-form">
    <div class="input-group mb-2">
      <form action="/avtocat/search" method="get">
        <input
          x-model="searchString"
          id="input-search"
          type="text"
          class="form-control mr-2"
          name="search_string"
          placeholder="Введите номер или наименование детали (не менее 3-х символов)..."
          value=""
        >
        <!--<div class="input-group-append">
          <button class="btn btn-outline-primary" type="submit" onclick="">Поиск</button>
        </div>-->
      </form>
    </div>
  </div>
  <div class="table-responsive-md acSearch__result" :class="show && 'showed'">
    <div x-show="!loading">
      <table class="table">
        <thead class="thead-light">
          <tr>
            <!--<th>Марка</th>-->
            <!--<th>Модель</th>-->
            <th>Иллюстрация</th>
            <th>Название</th>
            <th>Номер</th>
          </tr>
        </thead>
        <tbody>
          <template x-for="result in results">
            <tr>
              <!--<td><a x-bind:href="result.href" x-html="result.mark_name"></a></td>-->
              <!--<td><a x-bind:href="result.href" x-html="result.model_name"></a></td>-->
              <td><a x-bind:href="result.href" x-html="result.picture_name"></a></td>
              <td><a x-bind:href="result.href" x-html="result.name"></a></td>
              <td><a x-bind:href="result.href" x-html="result.number"></a></td>
            </tr>
          </template>
          <template x-if="results.length === 0">
            <tr class="acSearch__result__no-result">
              <td colspan="5">[[+noResultText]]</td>
            </tr>
          </template>
        </tbody>
      </table>
      <div class="acSearch__result__show-all">
        <a role="button" x-bind:href="linkWithSearchString" class="btn">Показать все</a>
      </div>
    </div>
    <div class="center-block acSearch__result__loader" x-show="loading">
      <svg
        style="position: absolute; width: 0; height: 0; overflow: hidden"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
        xmlns:xlink="http://www.w3.org/1999/xlink"
      >
        <defs>
          <symbol id="icon-spinner6" viewBox="0 0 32 32">
            <title>spinner6</title>
            <path d="M12 4c0-2.209 1.791-4 4-4s4 1.791 4 4c0 2.209-1.791 4-4 4s-4-1.791-4-4zM24.719 16c0 0 0 0 0 0 0-1.812 1.469-3.281 3.281-3.281s3.281 1.469 3.281 3.281c0 0 0 0 0 0 0 1.812-1.469 3.281-3.281 3.281s-3.281-1.469-3.281-3.281zM21.513 24.485c0-1.641 1.331-2.972 2.972-2.972s2.972 1.331 2.972 2.972c0 1.641-1.331 2.972-2.972 2.972s-2.972-1.331-2.972-2.972zM13.308 28c0-1.487 1.205-2.692 2.692-2.692s2.692 1.205 2.692 2.692c0 1.487-1.205 2.692-2.692 2.692s-2.692-1.205-2.692-2.692zM5.077 24.485c0-1.346 1.092-2.438 2.438-2.438s2.438 1.092 2.438 2.438c0 1.346-1.092 2.438-2.438 2.438s-2.438-1.092-2.438-2.438zM1.792 16c0-1.22 0.989-2.208 2.208-2.208s2.208 0.989 2.208 2.208c0 1.22-0.989 2.208-2.208 2.208s-2.208-0.989-2.208-2.208zM5.515 7.515c0 0 0 0 0 0 0-1.105 0.895-2 2-2s2 0.895 2 2c0 0 0 0 0 0 0 1.105-0.895 2-2 2s-2-0.895-2-2zM28.108 7.515c0 2.001-1.622 3.623-3.623 3.623s-3.623-1.622-3.623-3.623c0-2.001 1.622-3.623 3.623-3.623s3.623 1.622 3.623 3.623z"></path>
          </symbol>
        </defs>
      </svg>
      <span>
        <svg class="icon icon-spinner6 glyphicon-refresh-animate">
          <use xlink:href="#icon-spinner6"></use>
        </svg>
      </span>
      <span>Идет поиск ...</span>
    </div>
  </div>
</div>