import create from '@libs/wx-office';
import ValidatorPlugin from '@libs/wx-office/plugins/validator';
import EmailOrPhonePlugin from '@libs/wx-office/plugins/emailOrPhone';
import CustomNotifierPlugin from '@libs/wx-office/plugins/notifier';

const OfficeAuth = create();
const OfficeReg = create();
const OfficeResetPassword = create();

CustomNotifierPlugin.options = {
  life: 5000
};

ValidatorPlugin.options = {
  attrValidatorFieldName: 'field-name',
  replaceSiblingHelp: true,
  attrSiblingHelp: 'p.help-block > small'
};

OfficeAuth.use(CustomNotifierPlugin);
OfficeAuth.use(ValidatorPlugin);
OfficeAuth.use(EmailOrPhonePlugin);

OfficeReg.use(CustomNotifierPlugin);
OfficeReg.use(ValidatorPlugin);
OfficeReg.use(EmailOrPhonePlugin);

OfficeResetPassword.use(CustomNotifierPlugin);
OfficeResetPassword.use(ValidatorPlugin);
OfficeResetPassword.use(EmailOrPhonePlugin);

OfficeAuth.initialize('#office-auth-form form');
OfficeReg.initialize('#office-reg-form form');
OfficeResetPassword.initialize('#office-reset-form form');
