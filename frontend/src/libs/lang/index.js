import { template } from 'lodash';

const t = {
  ru: {
    'wx-office.required': 'Поле "<%= name.toLowerCase() %>" обязательно для заполнения!',
    'wx-office.isEmailOrPhone': 'Поле "<%= name.toLowerCase() %>" должно быть мобильным номером или email!',
    get 'wx-office.requiredIf'() {
      return this['wx-office.required'];
    },
    'wx-office.isLength': 'Поле "<%= name.toLowerCase() %>" содержит менее <%= result %> символов!',
    get 'wx-office.isLengthIf'() {
      return this['wx-office.isLength'];
    },
    'wx-office.check-form': 'Проверьте правильность заполнения формы',
    'wx-office.notifier.close-all': 'Закрыть все'
  }
};

export default function factory(lang, name, variables = {}) {
  return template(t?.[lang]?.[name] ?? '')(variables);
}
