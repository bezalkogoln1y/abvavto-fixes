<?php

if (!class_exists('SmsRu')) {
    require_once __DIR__ . '/smsru.class.php';
}

class File extends SmsRu
{
    public function __construct(modX $modx, array $config = array())
    {
        $this->logFilename = 'emulate-sms.office.log';

        parent::__construct($modx, $config);
    }

    /**
     * @param $phone
     * @param $text
     *
     * @return bool
     */
    public function send($phone, $text)
    {
        $this->debugLog(
            print_r([
                'phone' => $phone,
                'text' => $text
            ], true)
        );

        return true;
    }
}
