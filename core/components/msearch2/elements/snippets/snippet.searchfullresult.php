<?php
/** @var modX $modx */

$count = 0;
$defaultValue = 99999999;

$mSearchOutput = $modx->runSnippet('mSearch2', [
    'where' => json_encode(['template' => '4']),
    'fields' => 'pagetitle',
    'limit' => 0,
    // 'limit' => 2,
    'returnIds' => true
]);

if (!trim($mSearchOutput)) {
    $mSearchOutput = $defaultValue;
} else {
    $count = count(preg_split('#,#', $mSearchOutput));
}

$modx->setPlaceholder('mSearch.total', $count);

$output = $modx->runSnippet('pdoPage', [
    'element' => 'msProducts',
    'parents' => 0,
    'resources' => $mSearchOutput,
    'includeTVs' => 'ARTNUMBET_AVTOCATALOG',
    'tpl' => 'tpl.mSearch2.custom_row',
    'tplWrapper' => 'tpl.mSearch2.custom_wrapper',
]);

echo $output;

echo <<<END
<nav>
  {$modx->getPlaceholder('page.nav')}
</nav>
END;