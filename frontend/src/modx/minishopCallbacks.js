export default function minishopCallbacks($container) {
  miniShop2.Callbacks.add('Order.add.response.success', 'hide_delivery_cost', (res) => {
    let { delivery = '0' } = res?.data;
    delivery = Number(delivery);

    /** @type {JQuery<HTMLElement>} */
    const $msCostEl = $container;
    const $orderCostEl = $msCostEl.children('.order_cost');

    if (delivery && $msCostEl.length) {
      $msCostEl.children('.order_delivery_cost').css({
        display: delivery === 1 ? 'none' : 'block'
      });

      $msCostEl.children('.order_cart_cost').css({
        display: delivery === 1 ? 'none' : 'block'
      });

      if (delivery === 1) {
        $orderCostEl.addClass('js_hide-border');
        $orderCostEl.children('.title_cost').text('Итого');
      } else {
        $orderCostEl.removeClass('js_hide-border');
        $orderCostEl.children('.title_cost').text('Итого с доставкой');
      }
    }
  });
}
