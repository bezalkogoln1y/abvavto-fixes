<tr>
  <td>
    <a href="{$uri}">
      {if $thumb}
        <img
          src="{$thumb | phpthumbon: 'w=90&h=90&far=С&q=90&bg=ffffff'}" 
          class="img-fluid"
          alt="{$pagetitle}" 
          title="{$pagetitle}" 
          itemprop="image"
        />
      {else}
        <img 
          src="{'assets_url' | option}template/img/nophoto.jpg"
          class="img-fluid" 
          alt="{$pagetitle}" 
          title="{$pagetitle}"
        />
      {/if}
    </a>
  </td>
  <td><a href="{$uri}">{$pagetitle}</a></td>
  <td><a href="{$uri}">{$ARTNUMBET_AVTOCATALOG ?: $article}</a></td>
  <td><a href="/brands.html?brand={$vendor}">{$_pls['vendor.name']}</a></td>
  <td><a href="{$uri}">{$price} {'ms2_frontend_currency' | lexicon}</a></td>
</tr>
<!--msearch2_weight  ([[%mse2_weight]]: [[+weight]])-->
<!--msearch2_intro <p>[[+intro]]</p>-->