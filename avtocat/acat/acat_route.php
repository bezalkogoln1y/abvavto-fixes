<?php

class AcatRoute
{
    private $path = [];
    private $get = [];
    private $hru_cache = [];
    private $db;


    public function __construct($get, $db)
    {
        $this->db = $db;
        $this->get = $get;
        if (isset($get['r']) && rtrim($get['r'], '/') != '') {
            $route = explode('/', rtrim($get['r'], '/'));

            if (count($route) > 0) {
                $this->path[] = ACAT_HRU_ENABLED && !in_array($route[0], array(
                    'search',
                    'search_result',
                    'mark_filter',
                    'model_filter',
                    'details',
                    'part_usage',
                    'order',
                    'captcha'
                )) ? $this->getGroupId($route[0]) : $route[0];
            }
            if (count($route) > 1) {
                $this->path[] = ACAT_HRU_ENABLED ? $this->getMarkId($route[1]) : $route[1];
            }
            if (count($route) > 2) {
                $this->path[] = ACAT_HRU_ENABLED ? $this->getModelId($route[2]) : $route[2];
            }
            if (count($route) > 3) {
                $this->path[] = ACAT_HRU_ENABLED ? $this->getPictureId(
                    $this->path[2],
                    $route[3]
                ) : $route[3];
            }
        }
    }


    public function routeType()
    {
        switch (count($this->path)) {
            case 0:
                return AcatRouteType::MAIN;
            case 1:
                switch ($this->path[0]) {
                    case 'search':
                        return AcatRouteType::SEARCH;
                    case 'search_result':
                        return AcatRouteType::SEARCH_RESULT;
                    case 'search_result_json':
                        return AcatRouteType::SEARCH_RESULT_JSON;
                    case 'mark_filter':
                        return AcatRouteType::MARK_FILTER;
                    case 'model_filter':
                        return AcatRouteType::MODEL_FILTER;
                    case 'details':
                        return AcatRouteType::PART_DETAILS;
                    case 'part_usage':
                        return AcatRouteType::PART_USAGE;
                    case 'order':
                        return AcatRouteType::ORDER;
                    case 'captcha':
                        return AcatRouteType::CAPTCHA;
                    default:
                        return AcatRouteType::MARK;
                }
            case 2:
                return AcatRouteType::MODEL;
            case 3:
                return AcatRouteType::TREE;
            case 4:
                return AcatRouteType::PICTURE;
            default:
                return AcatRouteType::UNKNOWN;
        }
    }

    public function group()
    {
        return $this->path[0];
    }

    public function mark()
    {
        return $this->path[1];
    }

    public function model()
    {
        return $this->path[2];
    }

    public function picture()
    {
        return $this->path[3];
    }

    public function searchParams()
    {
        $search_params = array();
        $search_params['search_string'] = isset($this->get['search_string']) ? $this->get['search_string']
            : '';
        $search_params['page'] = isset($this->get['page']) ? $this->get['page'] : '1';
        $search_params['group_id'] = isset($this->get['group_id']) ? $this->get['group_id'] : '';
        $search_params['mark_id'] = isset($this->get['mark_id']) ? $this->get['mark_id'] : '';
        $search_params['model_id'] = isset($this->get['model_id']) ? $this->get['model_id'] : '';
        return $search_params;
    }

    public function params($name)
    {
        return isset($this->get[$name]) ? $this->get[$name] : '';
    }

    public function link($group = null, $mark = null, $model = null, $picture = null)
    {
        $result = '';
        if (isset($group)) {
            $result = $result . "/" . (ACAT_HRU_ENABLED ? $this->getGroupHru($group) : $group);
            if (isset($mark)) {
                $result = $result . "/" . (ACAT_HRU_ENABLED ? $this->getMarkHru($mark) : $mark);
                if (isset($model)) {
                    $result = $result . "/" . (ACAT_HRU_ENABLED ? $this->getModelHru($model) : $model);
                    if (isset($picture)) {
                        $result = $result . "/" . (ACAT_HRU_ENABLED ? $this->getPictureHru(
                                $model,
                                $picture
                            ) : $picture);
                    }
                }
            }
        }
        return ACAT_URL . $result;
    }


    private function getGroupHru($id)
    {
        $hru = $this->tryGetHru('group:' . $id);
        if ($hru) {
            return $hru;
        } else {
            $query = $this->db->query(
                'SELECT hru as hru ' . 'FROM ' . AC_DB_PREFIX . 'typeauto ' . 'WHERE id=' . $id
            )->row;
            $this->hru_cache['group:' . $id] = $query['hru'];
            return $query['hru'];
        }
    }

    private function getMarkHru($id)
    {
        $hru = $this->tryGetHru('mark:' . $id);
        if ($hru) {
            return $hru;
        } else {
            $query = $this->db->query(
                'SELECT hru as hru ' . 'FROM ' . AC_DB_PREFIX . 'marks ' . 'WHERE id=' . $id
            )->row;
            $this->hru_cache['mark:' . $id] = $query['hru'];
            return $query['hru'];
        }
    }

    private function getModelHru($id)
    {
        $hru = $this->tryGetHru('model:' . $id);
        if ($hru) {
            return $hru;
        } else {
            $query = $this->db->query(
                'SELECT hru as hru ' . 'FROM ' . AC_DB_PREFIX . 'models ' . 'WHERE id=' . $id
            )->row;
            $this->hru_cache['model:' . $id] = $query['hru'];
            return $query['hru'];
        }
    }

    private function getPictureHru($model_id, $picture_id)
    {
        $hru = $this->tryGetHru('picture:' . $model_id . ':' . $picture_id);
        if ($hru) {
            return $hru;
        } else {
            $query = $this->db->query(
                'SELECT hru as hru ' .
                'FROM ' .
                AC_DB_PREFIX .
                'grouptree ' .
                'WHERE IDMODEL=(SELECT BASIS_MODEL_ID FROM ' .
                AC_DB_PREFIX .
                'models WHERE ID = ' .
                (int)$model_id .
                ') ' .
                ' AND GROUPNO=' .
                $picture_id
            )->row;
            $this->hru_cache['picture:' . $model_id . ':' . $picture_id] = $query['hru'];
            return $query['hru'];
        }
    }

    private function getGroupId($hru)
    {
        $query = $this->db->query(
            'SELECT id as id ' . 'FROM ' . AC_DB_PREFIX . 'typeauto ' . 'WHERE hru="' . $hru . '"'
        )->row;
        $this->hru_cache['group:' . $query['id']] = $hru;
        return $query['id'];
    }

    private function getMarkId($hru)
    {
        $query = $this->db->query(
            'SELECT ID as id ' . 'FROM ' . AC_DB_PREFIX . 'marks ' . 'WHERE hru="' . $hru . '"'
        )->row;
        $this->hru_cache['mark:' . $query['id']] = $hru;
        return $query['id'];
    }

    private function getModelId($hru)
    {
        $query = $this->db->query(
            'SELECT ID as id ' . 'FROM ' . AC_DB_PREFIX . 'models ' . 'WHERE hru="' . $hru . '"'
        )->row;
        $this->hru_cache['model:' . $query['id']] = $hru;
        return $query['id'];
    }

    private function getPictureId($model_id, $picture_hru)
    {
        $query = $this->db->query(
            'SELECT GROUPNO as id ' .
            'FROM ' .
            AC_DB_PREFIX .
            'grouptree ' .
            'WHERE IDMODEL=(SELECT BASIS_MODEL_ID FROM ' .
            AC_DB_PREFIX .
            'models WHERE ID = ' .
            (int)$model_id .
            ') ' .
            ' AND hru="' .
            $picture_hru .
            '"'
        )->row;
        $this->hru_cache['picture:' . $model_id . ':' . $query['id']] = $picture_hru;
        return $query['id'];
    }

    private function tryGetHru($key)
    {
        return (array_key_exists(
            $key,
            $this->hru_cache
        )) ? $this->hru_cache[$key] : null;
    }
}

class AcatRouteType
{
    // PAGES
    const MAIN = 'MAIN';
    const MARK = 'MARK';
    const MODEL = 'MODEL';
    const TREE = 'TREE';
    const PICTURE = 'PICTURE';
    const SEARCH = 'SEARCH';
    // API
    const SEARCH_RESULT = 'SEARCH_RESULT';
    const MARK_FILTER = 'MARK_FILTER';
    const MODEL_FILTER = 'MODEL_FILTER';
    const PART_DETAILS = 'PART_DETAILS';
    const PART_USAGE = 'PART_USAGE';
    const ORDER = 'ORDER';
    const CAPTCHA = 'CAPTCHA';
    // OTHER
    const UNKNOWN = 'UNKNOWN';
    // JSON API
    public const SEARCH_RESULT_JSON = 'SEARCH_RESULT_JSON';
}
