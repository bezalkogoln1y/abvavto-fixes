export const timeout = ms => new Promise(resolve => setTimeout(resolve, ms));
export const wait = timeout;

export const createStubJGrowlNotifier = () => {
  const stub = {};
  const levels = ['error', 'success', 'close', 'info'];

  levels.forEach(level => {
    stub[level] = (...args) => console.log(...args);
  });

  return stub;
}
