const fs = require('fs');
const { default: common, resolveAssets } = require('../webpack.common');

async function cleanIfExist(name, ...args) {
  let jsFile, jsFileMap, cssFile, cssFileMap;

  switch (name) {
    case 'custom-auth':
      jsFile = resolveAssets('../components/office/js/auth/custom.js');
      jsFileMap = resolveAssets('../components/office/js/auth/custom.js.map');
      cssFile = resolveAssets('../components/office/css/auth/custom.css');
      cssFileMap = resolveAssets('../components/office/css/auth/custom.css.map');
      break;

    case 'custom-msearch2':
      jsFile = resolveAssets('../components/msearch2/js/web/custom.js');
      jsFileMap = resolveAssets('../components/msearch2/js/web/custom.js.map');
      // cssFile = resolveAssets('../components/office/css/auth/custom.css');
      // cssFileMap = resolveAssets('../components/office/css/auth/custom.css.map');
      break;

    default:
      jsFile = resolveAssets(`js/${ name }.js`);
      jsFileMap = resolveAssets(`js/${ name }.js.map`);
      cssFile = resolveAssets(`css/${ name }.css`);
      cssFileMap = resolveAssets(`css/${ name }.css.map`);
  }

  const existJsFile = fs.existsSync(jsFile);
  const existJsFileMap = fs.existsSync(jsFileMap);
  const existCssFile = fs.existsSync(cssFile);
  const existCssFileMap = fs.existsSync(cssFileMap);

  if (!existCssFile && !existJsFile) {
    console.log(`Js and css file not found! (${ name })`);
    return null;
  }

  if (existJsFile) {
    await fs.promises.unlink(jsFile);

    if (existJsFileMap) await fs.promises.unlink(jsFileMap);

    console.log(`Js file is removed (${ name })`);
  }

  if (existCssFile) {
    await fs.promises.unlink(cssFile);

    if (existCssFileMap) await fs.promises.unlink(cssFileMap);

    console.log(`Css file is removed (${ name })`);
  }
}

async function main(config) {
  console.log('RUN clean command');

  try {
    const entryFiles = Object.keys(config.entry);
    await Promise.all(entryFiles.map(cleanIfExist));

    console.log('SUCCESS clean command');
  } catch (error) {
    console.error('FAIL clean command. See error!!!');
    console.error(error);
  }
}

main(common({ dev: false }));
