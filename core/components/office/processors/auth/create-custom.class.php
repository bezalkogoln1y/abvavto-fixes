<?php

if (!class_exists('officeAuthUserCreateProcessor')) {
    require_once __DIR__ . '/create.class.php';
}

if (!class_exists('officeAuthUserCreateValidation')) {
    require_once __DIR__ . '/_validation.php';
}


class officeAuthUserCreateCustomProcessor extends officeAuthUserCreateProcessor
{
    public function beforeSave()
    {
        $this->addProfile();
        
        if ($this->modx->hasPermission('set_sudo')) {
            $sudo = $this->getProperty('sudo', null);
            if ($sudo !== null) {
                $this->object->setSudo(!empty($sudo));
            }
        }
        
        $this->validator = new officeAuthUserCreateValidation(
            $this, 
            $this->object, 
            $this->profile
        );
        $this->validator->validate();
        
        return !$this->hasErrors();
    }
}

return 'officeAuthUserCreateCustomProcessor';
