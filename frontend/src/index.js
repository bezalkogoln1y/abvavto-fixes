import minishopCallbacks from './modx/minishopCallbacks';
import './scss/main.scss';

const isDev = process.env.NODE_ENV === 'development';

$(() => {
  const $msCostEl = $('#msCost');

  if ($msCostEl.length) {
    minishopCallbacks($msCostEl);
  }

  $(document).on('after:af_success', (e, res) => {
    try {
      if (!res?.success) throw new Error(res?.message);

      /** @type {JQuery<HTMLFormElement>|null} */
      const $form = res?.form;

      if ($form) {
        if ($form.attr('id') === 'AjaxRecall') {
          $.magnificPopup.close();
        }
      }
    } catch (error) {
      if (isDev) console.error(error);
    }
  });

  $('a[href="#recall"]').magnificPopup({
    items: {
      type: 'inline',
      src: '.form__recall'
    },
    preload: false,
    modal: true,
    removalDelay: 300
  });
});
