<?php

if (!class_exists('SmsRu')) {
    require_once __DIR__ . '/smsru.class.php';
}

class SmsRuCall extends SmsRu
{
    public function __construct(modX $modx, array $config = array())
    {
        $this->logFilename = 'smsru-call.office.log';

        parent::__construct($modx, $config);
    }

    /**
     * @param $phone
     * @param $text
     *
     * @return bool
     */
    public function send($phone, $text = null)
    {
        $this->initializeHttpClient();

        function getIp()
        {
            $keys = [
                'HTTP_CLIENT_IP',
                'HTTP_X_FORWARDED_FOR',
                'REMOTE_ADDR'
            ];
            foreach ($keys as $key) {
                if (!empty($_SERVER[$key])) {
                    $ip = trim(end(explode(',', $_SERVER[$key])));
                    if (filter_var($ip, FILTER_VALIDATE_IP)) {
                        return $ip;
                    }
                }
            }
        }

        $ip = getIp();
        // выведем IP клиента на экран
        //echo 'ip = ' . $ip;

        $this->modx->log(modX::LOG_LEVEL_ERROR, '[Office] IP ' . $ip);

        $data = array(
            'api_id' => $this->modx->getOption('office_sms_id'),
            'phone' => $phone,
            'json' => '1',
            'ip' => $ip,
        );

        $res = $this->httpClient->get('https://sms.ru/code/call', $data);

        if ($res->responseError) {
            return false;
        }

        $resData = $res->process();

        if ($this->deepKeyExists($resData, 'status')) {
            $status = $resData['status'];

            if ($status === 'OK' && $this->deepKeyExists($resData, 'code')) {
                return $resData['code'];
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
