<form id="AjaxRecall" action="" method="post" class="ajax_form af_example" enctype="multipart/form-data">
  <div class="form-group">
    <label class="control-label" for="afRecallPhone">Телефон *</label>
    <div class="controls">
      <input id="afRecallPhone" type="text" name="phone" placeholder="" class="form-control phone__mask" />
      <span class="error_phone">[[+fi.error.phone]]</span>
    </div>
  </div>


  <div class="form-group">
    <div class="controls">
      <!--button type="reset" class="btn btn-default">[[%af_reset]]</button-->
      <button type="submit" class="btn btn-primary">[[%af_submit]]</button>
    </div>
  </div>

  [[+fi.success:is=`1`:then=`
  <div class="alert alert-success">[[+fi.successMessage]]</div>
  `]]

  [[+fi.validation_error:is=`1`:then=`
  <div class="alert alert-danger">[[+fi.validation_error_message]]</div>
  `]]
</form>