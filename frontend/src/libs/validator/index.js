import validator from 'validator';
import { isString, isNumber } from 'lodash';

function required(value) {
  if (isString(value)) {
    return value?.trim()?.length > 0;
  }

  if (isNumber(value)) {
    return value > 0;
  }

  return false;
}

function isEmailOrPhone(value) {
  return !!(
    validator.isMobilePhone(value) || validator.isEmail(value)
  );
}

function requiredIf(value, params, $el) {
  if (!window.$) return true;
  if (
    !(
      $el instanceof $
    )
  ) return true;

  const [condition, ...anyParams] = params.split(',');

  if (condition === 'choosenParentHasClass') {
    const [selector, className] = anyParams;

    if ($el.parents(selector).hasClass(className)) {
      return required(value);
    }

    return true;
  }
}

function isLength(value, length) {
  const result = validator.isLength(value, { min: +length });

  if (!result) return +length;
  return true;
}

function isLengthIf(value, params, $el) {
  if (!window.$) return true;
  if (
    !(
      $el instanceof $
    )
  ) return true;

  const [condition, ...anyParams] = params.split(',');

  if (condition === 'choosenParentHasClass') {
    const [selector, className, length] = anyParams;

    if ($el.parents(selector).hasClass(className)) {
      return isLength(value, length);
    }

    return true;
  }
}

export default {
  ...validator,
  required,
  isEmailOrPhone,
  requiredIf,
  isLength,
  isLengthIf
};
