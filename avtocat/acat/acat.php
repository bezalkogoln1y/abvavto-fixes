<?php

include('acat_config.php');
include('acat_route.php');
include('acat_pages.php');
include('acat_data.php');
include('acat_offers.php');
include('libs/acat_helpers.php');
include('libs/mysqli.php');
include('libs/captcha.php');

class Acat
{
    public function get_page_data()
    {
        $db = new \DB\MySQLi(
            ACAT_DB_HOSTNAME, ACAT_DB_USERNAME, ACAT_DB_PASSWORD, ACAT_DB_DATABASE
        );
        $route = new AcatRoute($_GET, $db);
        $acat_db = new AcatData($route, $db);
        $acat_pages = new AcatPages($route, $acat_db);

        $acat_data = [];
        $template = '';
        switch ($route->routeType()) {
            case (AcatRouteType::MAIN):
                $acat_data = $acat_pages->getGroupsData();
                $template = ACAT_ROOT . '/templates/groups.php';
                break;
            case (AcatRouteType::MARK):
                $acat_data = $acat_pages->getMarksData($route->group());
                $template = ACAT_ROOT . '/templates/marks.php';
                break;
            case (AcatRouteType::MODEL):
                $acat_data = $acat_pages->getModelsData(
                    $route->group(),
                    $route->mark()
                );
                $template = ACAT_ROOT . '/templates/models.php';
                break;
            case (AcatRouteType::TREE):
                $acat_data = $acat_pages->getTreeData(
                    $route->group(),
                    $route->mark(),
                    $route->model()
                );
                $template = ACAT_ROOT . '/templates/tree.php';
                break;
            case (AcatRouteType::PICTURE):
                $acat_data = $acat_pages->getPictureData(
                    $route->group(),
                    $route->mark(),
                    $route->model(),
                    $route->picture()
                );
                $template = ACAT_ROOT . '/templates/picture.php';
                break;
            case (AcatRouteType::SEARCH):
                $acat_data = $acat_pages->getSearchData($route->searchParams());
                $template = ACAT_ROOT . '/templates/search.php';
                break;
            default:
                throw new Exception('Ошибка в url!');
        }

        $data = array();
        $data['content'] = render_file($template, $acat_data);
        $data['title'] = $acat_data['title'];
        $data['header'] = $acat_data['header'];
        $data['meta_description'] = $acat_data['meta_description'];
        $data['meta_keywords'] = $acat_data['meta_keywords'];
        $data['breadcrumbs'] = array_merge($acat_data['breadcrumbs']);

        return $data;
    }

    public function api()
    {
        $db = new \DB\MySQLi(
            ACAT_DB_HOSTNAME, ACAT_DB_USERNAME, ACAT_DB_PASSWORD, ACAT_DB_DATABASE
        );
        $fts = null;
        if (ACAT_SPHINX_ENABLED) {
            $fts = new \DB\MySQLi(
                ACAT_SPHINX_HOSTNAME, ACAT_DB_USERNAME, ACAT_DB_PASSWORD, ACAT_SPHINX_INDEX, ACAT_SPHINX_PORT
            );
        }
        $route = new AcatRoute($_GET, $db);
        $acat_db = new AcatData($route, $db, $fts);

        switch ($route->routeType()) {
            case (AcatRouteType::PART_DETAILS):
                $model_id = $route->params('model_id');
                $part_id = $route->params('part_id');
                $result = $acat_db->getPartDetails($model_id, $part_id);
                header('Content-Type: application/json');
                return json_encode((array)$result);
            case (AcatRouteType::CAPTCHA):
                $capt = new Captcha();
                $capt->draw();
                return;
            case (AcatRouteType::ORDER):
                $result = sendOrder($_POST['order']);
                header('Content-Type: application/json');
                return json_encode($result);
            case (AcatRouteType::SEARCH_RESULT):
                $acat_data = $acat_db->getSearchResult($route->searchParams());
                $template = ACAT_ROOT . '/templates/search_result.php';
                return render_file(
                    $template,
                    $acat_data
                );
            case AcatRouteType::SEARCH_RESULT_JSON:
                $acat_data = $acat_db->getSearchResult($route->searchParams());
                header('Content-Type: application/json');
                return json_encode($acat_data);
            case (AcatRouteType::MARK_FILTER):
                $group_id = $route->params('group_id');
                $result = $acat_db->getMarks($group_id);
                header('Content-Type: application/json');
                return json_encode((array)$result);
            case (AcatRouteType::MODEL_FILTER):
                $group_id = $route->params('group_id');
                $mark_id = $route->params('mark_id');
                $result = $acat_db->getModels(
                    $group_id,
                    $mark_id
                );
                header('Content-Type: application/json');
                return json_encode((array)$result);
            default:
                throw new Exception('Ошибка в url!');
        }
    }
}
