import OfficeAuth from './core';

export function create() {
  return new OfficeAuth(window?.OfficeConfig ?? {});
}

export default create;
