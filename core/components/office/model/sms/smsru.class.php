<?php

if (!class_exists('officeUtils')) {
    require_once __DIR__ . '/../../utils/utils.class.php';
}

class SmsRu
{
    /** @var modX $modx */
    public $modx;

    /** @var modRest|null */
    protected $httpClient = null;

    /** @var officeUtils */
    private $officeUtils;


    /**
     * @param modX $modx
     * @param array $config
     */
    public function __construct(modX $modx, array $config = array())
    {
        $this->modx = &$modx;
        $this->officeUtils = new officeUtils($modx);
    }

    protected function debugLog($msg)
    {
        $this->officeUtils->debugLog($msg);
    }

    protected function initializeHttpClient()
    {
        $this->httpClient = $this->modx->getService('rest', 'rest.modRest');
        $this->httpClient->setOption('format', 'json');
        $this->httpClient->setOption('suppressSuffix', true);
    }

    protected function deepKeyExists($array, $path = null)
    {
        return $this->officeUtils->deepKeyExists($array, $path);
    }


    /**
     * @param $phone
     * @param $text
     *
     * @return mixed
     */
    public function send($phone, $text)
    {
        $this->initializeHttpClient();

        $testMode = $this->modx->getOption('office_sms_test_mode');
        $text = mb_convert_encoding($text, 'UTF-8');

        $data = array(
            'api_id' => $this->modx->getOption('office_sms_id'),
            'to' => $phone,
            'text' => $text,
            'json' => '1',
            'test' => $testMode
        );
        if ($from = trim($this->modx->getOption('office_sms_from'))) {
            $data['from'] = $from;
        }

        $res = $this->httpClient->get('https://sms.ru/sms/send', $data);

        if ($res->responseError) {
            return false;
        }

        $resData = $res->process();

        if ($this->deepKeyExists($resData, "sms.$phone.status_code")) {
            $statusCode = $resData['sms'][$phone]['status_code'];

            if ($testMode === '1') {
                return ['status' => $statusCode, 'text' => $text];
            }

            if ($statusCode !== 100) {
                return $statusCode;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
