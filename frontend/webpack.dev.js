const { merge } = require('webpack-merge');
const { default: common } = require('./webpack.common');

module.exports = env => {
  return merge(common(env), {

    mode: 'development',

    // Use eval-cheap-source-map for accurate line numbers, eval has best build performance
    devtool: 'source-map',

    output: {
      pathinfo: true,
      publicPath: '/'
    }
  });
};
