export const timeout = ms => new Promise(resolve => setTimeout(resolve, ms));
export const wait = timeout;
