import { defaults } from 'lodash';
import _t from '@libs/lang';

import './theme.scss';

/**
 * @typedef {Object} NotifierPluginOptions
 *
 * @property {string} [theme="custom-message"]
 * @property {number} [life=3000]
 */

/**
 * @param {NotifierPluginOptions} options
 * @this {OfficeAuth}
 */
function installNotifierPlugin(options = {}) {
  defaults(options, {
    theme: 'custom-message'
  });

  if (window?.$?.jGrowl) {
    $(function() {
      $.jGrowl.defaults.pool = 3;
      $.jGrowl.defaults.life = options.life;
      $.jGrowl.defaults.click = ($el) => {
        $el.jGrowl('close');
      };
      $.jGrowl.defaults.closerTemplate = `
      <div class="jGrowl-closer highlight ui-corner-all default jGrowl-closer__custom">
        ${ _t('ru', 'wx-office.notifier.close-all') }
      </div>
    `;
    });

    this._notifier = {
      success: function(message, sticky = null) {
        if (sticky === null) {
          sticky = false;
        }
        if (message) {
          $.jGrowl(message, {
            theme: `${ options.theme }-success`,
            sticky: sticky
          });
        }
      },
      error: function(message, sticky = null) {
        if (sticky === null) {
          sticky = false;
        }
        if (message) {
          $.jGrowl(message, {
            theme: `${ options.theme }-error`,
            sticky: sticky
          });
        }
      },
      info: function(message, sticky = null) {
        if (sticky === null) {
          sticky = false;
        }
        if (message) {
          $.jGrowl(message, {
            theme: `${ options.theme }-info`,
            sticky: sticky
          });
        }
      },
      close: function() {
        $.jGrowl('close');
      }
    };
  } else {
    console.error('[WxOffice > NotifierPlugin] jGrowl not install.');
  }
}

export const NotifierPlugin = {
  /**
   * @type {NotifierPluginOptions}
   */
  options: {},
  create() {
    return installNotifierPlugin.call(this, NotifierPlugin.options);
  }
};

export default NotifierPlugin;
