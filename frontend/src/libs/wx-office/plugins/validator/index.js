import { defaults } from 'lodash';
import coreValidator from '@libs/validator';
import _t from '@libs/lang';
import { wait } from '@libs/wx-utils';

/**
 * @typedef {Object} ValidatorPluginOptions
 *
 * @property {string} [inputSelector="input"]
 * @property {string} [attrValidatorBagName="validator"]
 * @property {string} [attrValidatorFieldName="validator-field-name"]
 * @property {boolean} [replaceSiblingHelp=false]
 * @property {string} [attrSiblingHelp="p.help-block"]
 * @property {number} [waitBeforeCloseAlert=0] deprecated
 */

/**
 * @param {ValidatorPluginOptions} options
 * @this {OfficeAuth}
 */
function installValidatorPlugin(options = {}) {
  defaults(options, {
    attrValidatorBagName: 'validator',
    attrValidatorFieldName: 'validator-field-name',
    replaceSiblingHelp: false,
    attrSiblingHelp: 'p.help-block',
    inputSelector: 'input:not([type="submit"])'
  });

  let rulesBag = [];

  const self = this;
  const requiredFields = [];

  /**
   * @param {JQuery<HTMLFormElement>} $form
   *
   * @return {JQuery<HTMLInputElement>}
   */
  function _getInputFields($form) {
    return $form.find('input')
      .filter(function() {
        return !!$(this).data(options.attrValidatorBagName);
      });
  }

  /**
   *
   * @param {string[]} validators
   * @param {string|null} name
   * @param {any} value
   * @param {JQuery<HTMLInputElement>} $input
   */
  function _validateField(validators, name, value, $input) {
    if (!validators.length) return [];

    const errors = [];

    validators.forEach((item) => {
      const [validatorName, validatorArguments = null] = item.split(':');
      const result = coreValidator[validatorName](value, validatorArguments, $input);

      if (result !== true) {
        errors.push(_t('ru', `wx-office.${ validatorName }`, { name, result }));
      }
    });

    return errors;
  }

  /**
   * @param {JQuery<HTMLInputElement>} $el
   * @this {OfficeAuth}
   *
   * @return {string[]}
   */
  function _checkValidation($el) {
    let errors = [];

    const validator = $el.data(options.attrValidatorBagName);
    const name = $el.data(options.attrValidatorFieldName) || null;
    const value = $el.val();
    const realFieldName = $el.attr('name');

    if ($el.attr('name') !== 'phone_code') {
      $el.removeClass('invalid');
    }

    if (validator?.trim()?.length) {
      const validators = validator.split('|');

      if (validators?.indexOf('required') !== -1) {
        requiredFields.push(realFieldName);
      }

      errors = _validateField(validators, name, value, $el);

      if (errors.length) {
        $el.addClass('invalid');

        if (options.replaceSiblingHelp) {
          const $sibling = $el.parent().find(options.attrSiblingHelp);

          if ($sibling.length) {
            $sibling.text(errors[0]);
          }
        }
      }
    }

    return errors;
  }

  self.onInitialize(function($el) {
    _getInputFields($el).each(function() {
      rulesBag.push($(this).data(options.attrValidatorBagName));
    });
  });

  self.onSubmit(function($el) {
    let allErrors = [];

    _getInputFields($el).each(function(idx) {
      const $input = $(this);

      $input.data(options.attrValidatorBagName, rulesBag[idx]);

      const errors = _checkValidation.call(self, $input);

      allErrors = allErrors.concat(errors);
    });

    if (allErrors.length) {
      self._notifier.error(_t('ru', 'wx-office.check-form'));

      self.toggleAllowAction(false);
    } else {
      self.toggleAllowAction(true);
    }
  });

  self.onPrependData(function(formData) {
    requiredFields.forEach(name => {
      const data = formData.find(item => item?.name === name);

      data.required = true;
    });
  });
}

export const ValidatorPlugin = {
  /**
   * @type {ValidatorPluginOptions}
   */
  options: {},
  create() {
    return installValidatorPlugin.call(this, ValidatorPlugin.options);
  }
};

export default ValidatorPlugin;
