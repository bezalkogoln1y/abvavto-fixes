const { merge } = require('webpack-merge');
const { default: common } = require('./webpack.common');

module.exports = env => {
  const config = common(env);

  return merge(config, {
    mode: 'production',

    // IMPORTANT: Configure server to disallow access to source maps from public!
    devtool: 'cheap-module-source-map',

    performance: {
      hints: false
    },

    plugins: []
  });
};
