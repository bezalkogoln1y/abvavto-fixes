const ERROR_CODE = 'WX_OFFICE_JQUERY_NOT_FOUND';

const translate = {
  ru: 'jQuery или $ не найдено в window',
  en: 'jQuery or $ not found in window'
}

export default class JqueryNotFoundError extends Error {
  constructor(lang = 'ru') {
    super(translate[lang]);

    this.errorCode = ERROR_CODE;

    Error.captureStackTrace(JqueryNotFoundError);
  }
}
