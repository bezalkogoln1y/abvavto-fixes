import JqueryNotFoundError from './errors/jquery-not-found.error';
import AlreadyInitializedError from './errors/already-initialized.error';
import { createStubJGrowlNotifier } from '@libs/wx-utils';

/**
 * @typedef {Object} WxOfficeAuthRequest
 *
 * @property {string} name
 * @property {boolean} required
 * @property {string} type
 * @property {any} value
 */

/**
 * @typedef {(this: typeof Office.Auth, $el: JQuery<HTMLFormElement>) => any} EventCallback
 */

/**
 * @typedef {(this: JQuery<HTMLFormElement>, response: object, status: number|string) => any} ResponseEventCallback
 */

/**
 * @typedef {(this: JQuery<HTMLFormElement>, formData: WxOfficeAuthRequest[], $el: JQuery<HTMLFormElement>) => any} PrependEventCallback
 */

/**
 * @typedef {Object} NotifierService
 *
 * @property {(message: string, sticky: boolean|null) => void} error
 * @property {(message: string, sticky: boolean|null) => void} info
 * @property {(message: string, sticky: boolean|null) => void} success
 * @property {() => void} close
 */

/**
 * @param {object} config
 *
 * @constructor
 */
function OfficeAuth(config) {
  /** @type {ResponseEventCallback[]} */
  this._errorCb = [];

  /** @type {ResponseEventCallback[]} */
  this._successCb = [];

  /** @type {EventCallback[]} */
  this._submitCb = [];

  /** @type {EventCallback[]} */
  this._initializeCb = [];

  /** @type {PrependEventCallback[]} */
  this._prependDataCb = [];

  /** @type {any[]} */
  this._plugins = [];

  /** @type {boolean} */
  this._allowAction = true;

  /** @type {boolean} */
  this._initialized = false;

  /** @type {{[k: string]: any}} */
  this._config = config;

  if (window?.Office?.Message) {
    /** @type {NotifierService} */
    this._notifier = window.Office.Message;
  } else {
    /** @type {NotifierService} */
    this._notifier = createStubJGrowlNotifier();
  }
}

/**
 *
 * @param {any} plugin
 */
OfficeAuth.prototype.use = function(plugin) {
  this._plugins.push(plugin);
};

/**
 *
 * @param {EventCallback} cb
 */
OfficeAuth.prototype.onInitialize = function(cb) {
  this._initializeCb.push(cb);
};

/**
 *
 * @param {EventCallback} cb
 */
OfficeAuth.prototype.onSubmit = function(cb) {
  this._submitCb.push(cb);
};

/**
 *
 * @param {PrependEventCallback} cb
 */
OfficeAuth.prototype.onPrependData = function(cb) {
  this._prependDataCb.push(cb);
};

/**
 *
 * @param {ResponseEventCallback} cb
 */
OfficeAuth.prototype.onError = function(cb) {
  this._errorCb.push(cb);
};

/**
 *
 * @param {ResponseEventCallback} cb
 */
OfficeAuth.prototype.onSuccess = function(cb) {
  this._successCb.push(cb);
};

OfficeAuth.prototype.toggleAllowAction = function(value = null) {
  this._allowAction = value === null ? !this._allowAction : value;
};

/**
 *
 * @param {string} selector
 * @return {boolean}
 *
 * @throws JqueryNotFoundError
 * @throws AlreadyInitializedError
 */
OfficeAuth.prototype.initialize = function(selector) {
  if (!window?.$) throw new JqueryNotFoundError();
  if (this._initialized) throw new AlreadyInitializedError();

  const elem = $(selector);
  const $document = $(document);
  const self = this;
  const config = this._config;

  this._plugins.forEach(plugin => plugin.create.call(self));
  this._initializeCb.forEach(cb => cb.call(self, elem));

  if (!elem.length) {
    return false;
  }

  $document.on('submit', selector, function(e) {
    e.preventDefault();

    self._submitCb.forEach((cb) => {
      cb.call(self, $(this));
    });

    if (self._allowAction) {
      $(this).ajaxSubmit({
        url: config.actionUrl,
        dataType: 'json',
        type: 'post',
        data: {
          pageId: self._config?.pageId ?? 1
        },
        beforeSubmit: function(formData, $form) {
          self._prependDataCb.forEach(cb => cb.call(self, formData, $form));

          // Additional check for old version of form
          let found = false;
          for (let i in formData) {
            if (formData.hasOwnProperty(i) && formData[i]['name'] == 'action') {
              found = true;
            }
          }
          if (!found) {
            formData.push({ name: 'action', value: 'auth/sendLink' });
          }
          // --
          $form.find('input, button, a').attr('disabled', true);

          return true;
        },
        success: function(response, status, xhr, $form) {
          $form.find('input, button, a').attr('disabled', false);
          if (response.success) {
            self._notifier.success(response.message);
            if (!response.data.sms) {
              $form.resetForm();
            }

            self._successCb.forEach(cb => cb.call($form, response, status));
          } else {
            self._notifier.error(response.message, false);
            self._errorCb.forEach(cb => cb.call($form, response, status));
          }
          if (response.data.sms != undefined) {
            if (response.data.sms == 1) {
              $form.find('input').attr('readonly', true);
              $form.find('[name="phone_code"]').attr('readonly', false)
                .parents('.hidden').removeClass('hidden').addClass('not-hidden');
            } else {
              $form.find('input').attr('readonly', false);
              $form.find('[name="phone_code"]').attr('readonly', true)
                .parents('.not-hidden').removeClass('not-hidden').addClass('hidden');
            }
          }

          if (response.data.refresh) {
            document.location.href = response.data.refresh;
          }
        }
      });
    }

    return false;
  });

  if (window?.Office?.Auth) {
    window.Office.Auth = this;
  }

  this._initialized = true;

  return true;
};

export default OfficeAuth;
