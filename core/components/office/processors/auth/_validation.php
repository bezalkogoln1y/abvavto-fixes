<?php

if (!class_exists('modUserValidation')) {
    require_once MODX_BASE_PATH . 'model/modx/processors/security/user/_validation.php';
}

class officeAuthUserCreateValidation extends modUserValidation
{
    public function checkEmail()
    {
        $email = $this->processor->getProperty('email');

        /*if (!$this->modx->getOption('allow_multiple_emails', null, true)) {
            // @var modUserProfile $emailExists
            $emailExists = $this->modx->getObject('modUserProfile', array('email' => $email));
            if ($emailExists) {
                if ($emailExists->get('internalKey') != $this->processor->getProperty('id')) {
                    $this->processor->addFieldError(
                        'email',
                        $this->modx->lexicon('user_err_already_exists_email')
                    );
                }
            }
        }*/
        
        return $email;
    }
}
