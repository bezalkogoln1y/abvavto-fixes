const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const dirNode = 'node_modules';
const dirApp = path.join(__dirname, 'src');
const dirAssets = path.join(__dirname, '../assets/template');
const resolveApp = (src) => path.join(dirApp, src ?? '');
const resolveAssets = (src) => path.join(dirAssets, src ?? '');

/**
 * Webpack Configuration
 */
module.exports.default = env => {
  // Is the current build a development build
  const IS_DEV = !!env.dev;

  return {

    entry: {
      'custom-app': resolveApp('index.js'),
      'ajax-form': resolveApp('modx/ajaxForm.js'),
      'custom-avtocat': resolveApp('avtocat/index.js'),
      'custom-auth': resolveApp('modx/officeAuth.js'),
      'custom-msearch2': resolveApp('modx/msearch2')
    },

    output: {
      filename: (pathInfo) => {
        if (pathInfo.runtime === 'custom-auth') {
          return '../components/office/js/auth/custom.js';
        }

        if (pathInfo.runtime === 'custom-msearch2') {
          return '../components/msearch2/js/web/custom.js';
        }

        return 'js/[name].js';
      },
      path: resolveAssets()
    },

    resolve: {
      modules: [
        dirNode,
        dirApp
      ],
      alias: {
        '@': resolveApp(),
        '@libs': resolveApp('libs'),
        '@modx': resolveApp('modx'),
        '@officeComponent': resolveAssets('../components/office'),
        '@msearch2Component': resolveAssets('../components/msearch2')
      }
    },

    plugins: [
      new webpack.DefinePlugin({ IS_DEV }),
      new MiniCssExtractPlugin({
        filename: (fileInfo) => {
          if (fileInfo) {
            if (fileInfo.chunk) {
              if (fileInfo.chunk.name === 'custom-auth') {
                return '../components/office/css/auth/custom.css';
              }
            }
          }

          return 'css/[name].css';
        }
      })
    ],

    module: {
      rules: [
        // BABEL
        {
          test: /\.m?js$/,
          exclude: /(node_modules)/,
          use: {
            loader: 'babel-loader',
            options: {
              compact: true
            }
          }
        },

        // CSS / SASS
        {
          test: /\.s[ac]ss/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {}
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: IS_DEV
              }
            },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: IS_DEV,
                sassOptions: {
                  includePaths: [dirAssets]
                }
              }
            }
          ]
        }
      ]
    }
  };
};

module.exports.resolveApp = resolveApp;
module.exports.resolveAssets = resolveAssets;
