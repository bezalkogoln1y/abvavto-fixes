const ERROR_CODE = 'WX_OFFICE_ALREADY_INITIALIZED';

const translate = {
  ru: 'OfficeAuth уже успешно проиницилизирован',
  en: 'OfficeAuth already initialized'
};

export default class AlreadyInitializedError extends Error {
  constructor(lang = 'ru') {
    super(translate[lang]);

    this.errorCode = ERROR_CODE;

    Error.captureStackTrace(AlreadyInitializedError);
  }
}
