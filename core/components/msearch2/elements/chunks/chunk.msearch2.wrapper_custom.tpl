{var $total = $_modx->getPlaceholder('mSearch.total')}

<div class="appSearchResults">
  <table class="table">
    <thead class="thead-light">
      <tr>
        <th>Фото</th>
        <th>Название</th>
        <th>Артикул</th>
        <th>Производитель</th>
        <th>Цена</th>
      </tr>
    </thead>
    <tbody>
        {$output}
    </tbody>
    <tfoot>
      <tr>
        {if $total > 0}
          <td colspan="5">Найдено деталей: {$total}</td>
        {else}
          <td colspan="5">По вашему запросу ничего не найдено. Попробуйте другой!</td>
        {/if}
      </tr>
    </tfoot>
  </table>
</div>