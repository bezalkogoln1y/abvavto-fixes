class MessageService {
  static success(message, sticky) {
    if (message) {
      $.jGrowl(message, { theme: 'af-message-success', sticky: !!sticky });
    }
  }

  static error(message, sticky) {
    if (message) {
      $.jGrowl(message, { theme: 'af-message-error', sticky: !!sticky });
    }
  }

  static info(message, sticky) {
    if (message) {
      $.jGrowl(message, { theme: 'af-message-info', sticky: !!sticky });
    }
  }

  static close() {
    $.jGrowl('close');
  }

  static _registerCloserTemplate(closeMessage) {
    document.addEventListener('DOMContentLoaded', () => {
      $.jGrowl.defaults.closerTemplate = [
        '<div>[ ',
        closeMessage,
        ' ]</div>'
      ].join('');
    });
  }
}

class AjaxForm {
  /**
   * Сервис для формирования сообщений
   *
   * @type {typeof MessageService}
   */
  static Message = MessageService;

  /**
   *
   * @param {Object} config Конфигурация AjaxForm
   */
  constructor(config) {
    this._config = config;
  }

  /**
   * Инициализация AjaxForm
   *
   * @param {Object} config Конфигурация AjaxForm
   */
  static initialize(config) {
    if (!window.jQuery) return;

    const instance = new this(config);

    AjaxForm.Message._registerCloserTemplate(instance?._config?.closeMessage);

    /** @type {JQuery<HTMLFormElement>} */
    const $formEl = $(instance?._config?.formSelector);

    /** @type {JQuery<HTMLElement>} */
    const $errorEl = $('.error');

    instance._registerLibs();

    instance._registerFormEvent($formEl);
    instance._registerErrorEvent($errorEl);
  }

  /**
   * Добавление зависимостей в конец HTML
   *
   * @param {string} name Наименование файла (расширение всегда .js)
   * @param {string} assetsUrl Путь от которого искать (обычно [[++assetsUrl]])
   * @private
   */
  _appendLib(name, assetsUrl) {
    const el = document.createElement('script');
    el.src = `${ assetsUrl.replace(/(.*)\/$/, '$1') }/js/lib/${ name }.js`;

    document.append(el);
  }

  /**
   * Регистрация нужных зависимостей
   *
   * @private
   */
  _registerLibs() {
    const { assetsUrl = null } = this._config;

    if (!assetsUrl) {
      console.error('AjaxForm._registerLibs()', 'Setting assetsUrl in config not found');
    }

    if (!window.jQuery().ajaxForm) {
      this._appendLib('jquery.form.min', assetsUrl);
    }

    if (!window.jQuery().jGrowl) {
      this._appendLib('jquery.jgrowl.min.js');
    }
  }

  /**
   * Регистрация слушателей событий для формы (submit, reset)
   *
   * @param {JQuery<HTMLFormElement>} $formEl
   * @private
   */
  _registerFormEvent($formEl) {
    const { pageId, actionUrl } = this._config;
    const $document = $(document);

    $formEl.off('submit').on('submit', function (e) {
      $(this).ajaxSubmit({
        dataType: 'json',
        data: { pageId },
        url: actionUrl,
        beforeSerialize: function (form) {
          form.find(':submit').each(function () {
            if (!form.find('input[type="hidden"][name="' + $(this).attr('name') + '"]').length) {
              $(form).append(
                $('<input type="hidden">').attr({
                  name: $(this).attr('name'),
                  value: $(this).attr('value')
                })
              );
            }
          });
        },
        beforeSubmit: function (fields, form) {
          //noinspection JSUnresolvedVariable
          if (typeof (afValidated) != 'undefined' && !!afValidated === false) {
            return false;
          }
          form.find('.error').html('');
          form.find('.error').removeClass('error');
          form.find('input,textarea,select,button').attr('disabled', true);
          return true;
        },
        success: function (response, status, xhr, form) {
          form.find('input,textarea,select,button').attr('disabled', false);

          response.form = form;

          $document.trigger('af_complete', response);

          if (!response.success) {
            $document.trigger('before:af_error', response);

            AjaxForm.Message.error(response.message);
            if (response.data) {
              let key, value, focused;

              for (key in response.data) {
                if (response.data.hasOwnProperty(key)) {
                  if (!focused) {
                    form.find('[name="' + key + '"]').focus();
                    focused = true;
                  }
                  value = response.data[key];
                  form.find('.error_' + key).html(value).addClass('error');
                  form.find('[name="' + key + '"]').addClass('error');
                }
              }
            }

            $document.trigger('after:af_error', response);
          } else {
            $document.trigger('before:af_success', response);

            AjaxForm.Message.success(response.message);
            form.find('.error').removeClass('error');
            form[0].reset();

            //noinspection JSUnresolvedVariable
            if (typeof (grecaptcha) != 'undefined') {
              //noinspection JSUnresolvedVariable
              grecaptcha.reset();
            }

            $document.trigger('after:af_success', response);
          }
        }
      });

      e.preventDefault();
      return false;
    });

    $formEl.on('reset', function () {
      $(this).find('.error').html('');

      AjaxForm.Message.close();
    });
  }

  /**
   * Регистрация слушаетеля событий для .error (keypress, change)
   *
   * @param {JQuery<HTMLElement>} $errorEl
   * @private
   */
  _registerErrorEvent($errorEl) {
    $errorEl.on('keypress change', function () {
      const key = $(this).attr('name');
      $(this).removeClass('error');

      $('.error_' + key).html('').removeClass('error');
    });
  }
}

window.AjaxForm = AjaxForm;
