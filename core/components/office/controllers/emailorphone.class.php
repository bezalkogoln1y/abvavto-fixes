<?php

declare(strict_types=1);

if (!class_exists('officeAuthController')) {
    require_once __DIR__ . '/auth.class.php';
}

if (!class_exists('officeUtils')) {
    require_once __DIR__ . '/../utils/utils.class.php';
}

class officeEmailOrPhoneController extends officeAuthController
{
    private const E_SMS_PROVIDER = 'sms_provider_error';
    private const E_REGISTER = 'modRegister_error';

    private officeUtils $officeUtils;

    public function __construct(Office $office, array $config = array())
    {
        parent::__construct($office, $config);
        $this->officeUtils = new officeUtils($this->modx);
    }

    private function debugLog($msg)
    {
        $this->officeUtils->debugLog($msg, 'emailorphone.debug.log');
    }

    /**
     * Проверить поле на валидность
     *
     * @param string|null $value
     *
     * @return string
     */
    private function checkField($value = null)
    {
        if (!$value || gettype($value) !== 'string') {
            return '';
        }

        return trim($value);
    }

    /**
     * Проверить числовое поле на валидность
     *
     * @param string|null $value
     *
     * @return string
     */
    private function checkNumericField($value = null)
    {
        if (!$value || !is_numeric($value)) {
            return '';
        }

        return $value;
    }

    /**
     * @param array $data
     *
     * @return array|bool|string|null
     */
    public function formLogin($data = [])
    {
        // Check user status
        if ($this->modx->user->isAuthenticated($this->modx->context->key)) {
            return $this->success($this->modx->lexicon('office_auth_err_already_logged'), array(
                'refresh' => $this->_getLoginLink(),
                'reload' => !$this->_getLoginLink()
            ));
        }

        // Checks
        $username = strtolower($this->checkField($data['username']));
        $email = strtolower($this->checkField($data['email']));
        $phone = $this->office->checkPhone(!empty($username) ? $username
            : $this->checkField($data['mobilephone']));

        if (!empty($username)) {
            if ($user = $this->modx->getObject('modUser', array('username' => $username))) {
                $profile = $user->getOne('Profile');
            } else {
                $profile = $this->modx->getObject('modUserProfile', array('email' => $username));
            }
        } elseif (!empty($email)) {
            $this->modx->setOption('office_auth_mode', 'email');
            $profile = $this->modx->getObject('modUserProfile', array('email' => $email));
        } elseif (!empty($phone)) {
            $this->modx->setOption('office_auth_mode', 'phone');
            $profile = $this->modx->getObject('modUserProfile', array('mobilephone' => $phone));
        } else {
            return $this->error($this->modx->lexicon('office_auth_err_email_username_ns'));
        }

        /** @var modUserProfile $profile */
        if (!empty($profile) && $profile instanceof modUserProfile) {
            /** @var modUser $user */
            $user = $profile->getOne('User');
            $password = $this->checkField($data['password']);
            $phoneCode = $this->checkNumericField($data['phone_code']);

            // If user did not activate his account - try to send link again
            if (!$user->get('active') && empty($phoneCode)) {
                $reset = $this->_resetPassword($user->username);
                if (!empty($this->config['json_response'])) {
                    $reset = json_decode($reset, true);
                }
                if ($reset['success']) {
                    if (empty($reset['message'])) {
                        $reset['message'] = $this->modx->lexicon('office_auth_err_user_active');
                    }

                    return $this->success($reset['message'], $reset['data']);
                } else {
                    return $this->error($reset['message'], $reset['data']);
                }
            } // If user did not send password - he wants to reset it
            elseif (empty($password)) {
                if ($this->modx->getOption('office_auth_mode') == 'phone') {
                    if (!empty($phoneCode)) {
                        $data['mobilephone'] = $profile->mobilephone;

                        return $this->Login($data);
                    }
                }

                return $this->_resetPassword($user->username);
            }

            // Otherwise we try to login
            $login_data = array(
                'username' => $user->get('username'),
                'password' => $password,
                'rememberme' => $this->config['rememberme'],
                'login_context' => $this->config['loginContext'],
            );
            if (!empty($this->config['addContexts'])) {
                $login_data['add_contexts'] = $this->config['addContexts'];
            }

            $this->modx->log(modX::LOG_LEVEL_INFO, $this->_getLoginLink());

            $response = $this->modx->runProcessor('security/login', $login_data);
            if ($response->isError()) {
                $errors = $this->_formatProcessorErrors($response);
                $this->modx->log(
                    modX::LOG_LEVEL_INFO,
                    '[Office] unable to login user "' . $data['username'] . '". Message: ' . $errors
                );

                return $this->error($this->modx->lexicon(
                    'office_auth_err_login',
                    array('errors' => $errors)
                ));
            } else {
                return $this->success($this->modx->lexicon('office_auth_success'), array(
                    'refresh' => $this->_getLoginLink(),
                    'reload' => !$this->_getLoginLink()
                ));
            }
        } else {
            return $this->error($this->modx->lexicon('office_auth_err_email_username_nf'));
        }
    }

    /**
     * Create new user and confirm his email
     *
     * @param $data
     *
     * @return array|string
     */
    public function formRegister($data)
    {
        // Check user status
        if ($this->modx->user->isAuthenticated($this->modx->context->key)) {
            return $this->success(
                $this->modx->lexicon('office_auth_err_already_logged'),
                array('refresh' => $this->_getLoginLink())
            );
        }

        // Checks
        $password = $this->checkField($data['password']);
        $email = strtolower($this->checkField($data['email']));
        $mobilephone = $this->office->checkPhone($this->checkField($data['mobilephone']));
        $username = strtolower($this->checkField($data['username']));
        $fullname = $this->modx->stripTags($this->checkField($data['fullname']));

        // Check password
        if (!empty($password)) {
            $req = $this->modx->getOption('password_min_length', null, 6);
            if (strlen($password) < $req) {
                return $this->error($this->modx->lexicon(
                    'office_auth_err_password_short',
                    array('req' => $req)
                ));
            } elseif (!preg_match('/^[^\'\\x3c\\x3e\\(\\);\\x22]+$/', $password)) {
                return $this->error($this->modx->lexicon('office_auth_err_password_invalid'));
            }
        }

        if (!empty($mobilephone)) {
            if ($this->modx->getOption('office_sms_problem') == 1) {
                return $this->error($this->modx->lexicon('office_auth_err_phone_not_available'));
            }

            $this->modx->setOption('office_auth_mode', 'phone');
            /*if (!$mobilephone) {
                return $this->error($this->modx->lexicon('office_auth_err_phone_invalid'));
            }*/
            if (!empty($data['phone_code']) && is_numeric($data['phone_code'])) {
                return $this->Login($data);
            } elseif ($this->modx->getCount('modUserProfile', array('mobilephone' => $mobilephone))) {
                return $this->error($this->modx->lexicon('office_auth_err_phone_exists'));
            }
        } else {
            $this->modx->setOption('office_auth_mode', 'email');
            /*if (empty($email)) {
                return $this->error($this->modx->lexicon('office_auth_err_email_ns'));
            }*/
            $exists = $this->modx->getCount('modUserProfile', array('email' => $email)) ||
            $this->modx->getCount('modUser', array('username' => $email));
            if ($exists) {
                return $this->error($this->modx->lexicon('office_auth_err_email_exists'));
            }
        }

        // Check username
        if (!empty($username)) {
            if (!preg_match('/^[^\'\\x3c\\x3e\\(\\);\\x22]+$/', $username)) {
                return $this->error($this->modx->lexicon('office_auth_err_username_invalid'));
            } elseif ($this->modx->getCount('modUser', array('username' => $username))) {
                return $this->error($this->modx->lexicon('office_auth_err_username_exists'));
            }
        } elseif (!empty($email)) {
            $username = $email;
        } elseif (!empty($mobilephone)) {
            $username = $mobilephone;
        }

        return $this->_createUser(array(
            'username' => $username,
            'email' => $email,
            'mobilephone' => $mobilephone,
            'password' => $password,
            'fullname' => $fullname,
        ));
    }

    /**
     * @param array $data
     *
     * @return array|bool|string|null
     */
    public function formReset($data = [])
    {
        $phone = $this->checkField($data['mobilephone']);

        if (!empty($phone)) {
            if ($this->modx->getOption('office_sms_problem') == 1) {
                return $this->error($this->modx->lexicon('office_auth_err_phone_not_available_reset'));
            }
        }

        if ($this->modx->getOption('office_force_to_profile') === '1') {
            $_REQUEST['pageId'] = $this->modx->getOption('office_profile_page_id');
        }

        return $this->formLogin($data);
    }

    /**
     * Generate new password and send activation link for existing user
     *
     * @param $username
     * @param $password
     * @param $tpl
     *
     * @return array|string
     */
    protected function _resetPassword($username, $password = '', $tpl = '')
    {
        /** @var modUser $user */
        $c = $this->modx->newQuery('modUser');
        $c->innerJoin('modUserProfile', 'Profile');
        if (!empty($username)) {
            $c->where(array('modUser.username' => $username, 'OR:Profile.email:=' => $username));
        } else {
            return $this->error($this->modx->lexicon('office_auth_err_email_username_ns'));
        }
        $c->select($this->modx->getSelectColumns('modUser', 'modUser') .
                   ',`Profile`.`email`,`Profile`.`mobilephone`');
        if (!$user = $this->modx->getObject('modUser', $c)) {
            return $this->error($this->modx->lexicon('office_auth_err_email_username_nf'));
        }
        /*
        elseif ($user->sudo) {
            return $this->error($this->modx->lexicon('office_auth_err_sudo_user'));
        }
        */
        $email = $user->get('email');
        $phone = $user->get('mobilephone');
        if (
            $this->modx->getOption('office_auth_mode') == 'phone' &&
            $phone = $this->office->checkPhone($phone)
        ) {
            $mode = 'phone';
            $activationHash = rand(100000, 999999);

            // FIXME: Включить, если снова понадобится по звонку
            // if (strtolower($this->modx->getOption('office_sms_provider')) === 'smsru') {
            //     return $this->callPhoneResetPassword($user, $password, $phone);
            // }
        } else {
            $mode = 'email';
            $activationHash = md5(uniqid(md5($email . '/' . $user->get('id')), true));
        }

        /** @var modDbRegister $registry */
        $registry = $this->modx->getService('registry', 'registry.modRegistry')
            ->getRegister('user', 'registry.modDbRegister');
        $registry->connect();

        // Check if we already sent activation link
        $registry->subscribe('/pwd/reset/' . md5($user->username));
        $res = $registry->read(array('poll_limit' => 1, 'remove_read' => false));
        if (!empty($res)) {
            return $this->error(
                $this->modx->lexicon('office_auth_err_already_' . $mode . '_sent'),
                array('sms' => $mode == 'phone')
            );
        }

        $newPassword = !empty($password) ? $password : $user->generatePassword();
        $_SESSION['Office']['Auth']['cachepwd'] = $newPassword;
        if ($profile = $user->getOne('Profile')) {
            $extended = $profile->get('extended');
            $extended['office_activation_key'] = $activationHash;
            $profile->set('extended', $extended);
        }
        $user->set('cachepwd', $newPassword);
        $user->save();

        // Render activation template
        if (empty($tpl)) {
            $tpl = $this->config['tplActivate'];
        }
        $pls = array_merge($user->getOne('Profile')->toArray(), $user->toArray());
        if ($mode == 'phone') {
            $pls['code'] = $activationHash;
            $pls['link'] = '';
        } else {
            $id = !empty($this->config['loginResourceId']) ? $this->config['loginResourceId']
            : (!empty($_REQUEST['pageId']) ? $_REQUEST['pageId'] : $this->modx->getOption('site_start'));
            $pls['code'] = '';
            $pls['link'] = $this->modx->makeUrl($id, '', array(
                'action' => 'auth/login',
                'email' => rawurlencode($email),
                'hash' => $activationHash . ':' . $newPassword,
            ), 'full');
        }
        $pls['password'] = $newPassword;

        $content = $this->office->getChunk($tpl, $pls);
        $maxIterations = (int)$this->modx->getOption('parser_max_iterations', null, 10);
        $this->modx->getParser()
            ->processElementTags('', $content, false, false, '[[', ']]', array(), $maxIterations);
        $this->modx->getParser()
            ->processElementTags('', $content, true, true, '[[', ']]', array(), $maxIterations);

        if ($mode == 'phone') {
            /** @var SmsRu|ByteHand $provider */
            $provider = $this->modx->getService(
                $this->modx->getOption('office_sms_provider'),
                $this->modx->getOption('office_sms_provider'),
                $this->modx->getOption(
                    'office_sms_provider_path', null,
                    $this->office->config['corePath'] . 'model/sms/'
                )
            );
            if (is_object($provider) && method_exists($provider, 'send')) {
                $send = $provider->send($phone, trim($content));
                if ($send !== true) {
                    $this->modx->log(
                        modX::LOG_LEVEL_ERROR,
                        '[Office] Unable to send sms to ' . $phone . '. Response is: ' . $send
                    );

                    if (is_array($send)) {
                        $this->debugLog([
                            'type' => 'test.smsru',
                            'code' => $activationHash,
                            'status' => $send['status'],
                            'text' => $send['text']
                        ]);
                    }

                    return $this->error($this->modx->lexicon(
                        'office_auth_err_sms',
                        array('errors' => is_array($send) ? 'Тестовый режим' : $send)
                    ));
                }
            } else {
                return $this->error($this->modx->lexicon('office_auth_err_sms_provider'));
            }
        } else {
            $send = $user->sendEmail(trim($content), array(
                'subject' => $this->modx->lexicon('office_auth_email_subject'),
            ));
            if ($send !== true) {
                $errors = $this->modx->mail->mailer->ErrorInfo;
                $this->modx->log(
                    modX::LOG_LEVEL_ERROR,
                    '[Office] Unable to send email to ' . $email . '. Message: ' . $errors
                );

                return $this->error($this->modx->lexicon(
                    'office_auth_err_email_send',
                    array('errors' => $errors)
                ));
            }
        }

        $registry->subscribe('/pwd/reset/');
        $result = $registry->send(
            '/pwd/reset/', array(md5($user->username) => $activationHash),
            array('ttl' => $this->config['linkTTL'])
        );

        if (!$result) {
            $this->error($this->modx->lexicon('office_err_action_nf'));
        }

        if (!$registry->close()) {
            $this->error($this->modx->lexicon('office_err_action_nf'));
        }

        return $mode == 'phone' ? $this->success(
            $this->modx->lexicon('office_auth_phone_send'),
            array('sms' => true)
        ) : $this->success($this->modx->lexicon('office_auth_email_send'));
    }

    /**
     * {@inheritDoc}
     */
    protected function _createUser($data)
    {
        $forceEmail = $this->modx->getOption('office_force_email_from_phone') === '1';

        if (empty($data['email']) && $forceEmail) {
            $data['email'] = $data['username'] . '@' . $this->modx->getOption('http_host');
        }
        if (empty($data['fullname']) && !empty($data['email'])) {
            $data['fullname'] = substr($data['email'], 0, strpos($data['email'], '@'));
        }
        if (empty($data['mobilephone'])) {
            $data['mobilephone'] = '';
        }

        $response = $this->office->runProcessor('auth/create-custom', array(
            'username' => $data['username'],
            'fullname' => $data['fullname'],
            'mobilephone' => $data['mobilephone'],
            'email' => $data['email'],
            'active' => false,
            'blocked' => false,
            'groups' => $this->config['groups'],
        ));

        if ($response->isError()) {
            $errors = $this->_formatProcessorErrors($response);

            $this->debugLog(new Exception("[Office] Unable to create user {$data['username']}. Message: $errors"));

            return $this->error($this->modx->lexicon('office_auth_err_create', array('errors' => $errors)));
        }


        if ($this->modx->getOption('office_force_to_profile') === '1') {
            $_REQUEST['pageId'] = $this->modx->getOption('office_profile_page_id');
        }

        return $this->_resetPassword($data['username'], $data['password'], $this->config['tplRegister']);
    }

    /**
     * @param modUser|null $user
     * @param string|null $password
     * @param string|null $phone
     */
    private function callPhoneResetPassword($user = null, $password = null, $phone = null)
    {
        /** @var modDbRegister $registry */
        $registry = $this->modx->getService('registry', 'registry.modRegistry')
            ->getRegister('user', 'registry.modDbRegister');

        if (!$registry->connect()) {
            $this->debugLog([
                'code' => self::E_REGISTER,
                'error' => 'modDbRegistrer не может соеденится...'
            ]);
            $this->error($this->modx->lexicon('office_err_action_nf'));
        }

        // Check if we already sent activation link
        $registry->subscribe('/pwd/reset/' . md5($user->username));
        $res = $registry->read(array('poll_limit' => 1, 'remove_read' => false));
        if (!empty($res)) {
            return $this->error($this->modx->lexicon('office_auth_err_already_phone_call'), ['sms' => true]);
        }

        /** @var SmsRuCall|null $provider */
        $provider = $this->modx->getService(
            'smsrucall', 'smsrucall',
            $this->modx->getOption(
                'office_sms_provider_path', null,
                $this->office->config['corePath'] . 'model/sms/'
            )
        );

        if (!is_object($provider) || !method_exists($provider, 'send')) {
            $this->debugLog([
                'code' => self::E_SMS_PROVIDER,
                'error' => 'Метод send или провайдер не найдены',
                'payload' => [
                    'name' => 'smsrucall',
                    'method' => 'send'
                ]
            ]);
            return $this->error($this->modx->lexicon('office_auth_err_sms_provider'));
        }

        $send = $provider->send($phone);
        if (empty($send)) {
            $this->modx->log(
                modX::LOG_LEVEL_ERROR,
                '[Office] Unable to call to ' . $phone . '. Response is: ' . $send
            );

            return $this->error($this->modx->lexicon('office_auth_err_call', ['errors' => $send]));
        }

        $activationHash = $send;

        $newPassword = !empty($password) ? $password : $user->generatePassword();
        $_SESSION['Office']['Auth']['cachepwd'] = $newPassword;

        $user->set('cachepwd', $newPassword);
        if ($profile = $user->getOne('Profile')) {
            $extended = $profile->get('extended');
            $extended['office_activation_key'] = $activationHash;
            $profile->set('extended', $extended);
        }
        $user->save();

        if (!$registry->subscribe('/pwd/reset/')) {
            $this->error($this->modx->lexicon('office_err_action_nf'));
        }
        $result = $registry->send(
            '/pwd/reset/', array(md5($user->username) => $activationHash),
            array('ttl' => $this->config['linkTTL'])
        );

        if (!$result) {
            $this->error($this->modx->lexicon('office_err_action_nf'));
        }

        if (!$registry->close()) {
            $this->error($this->modx->lexicon('office_err_action_nf'));
        }

        return $this->success($this->modx->lexicon('office_auth_phone_call'), ['sms' => true]);
    }
}

return 'officeEmailOrPhoneController';
