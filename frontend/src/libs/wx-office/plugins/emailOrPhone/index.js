import coreValidator from '@libs/validator';

function _checkType(value) {
  if (coreValidator.isEmail(value)) return 'email';
  if (coreValidator.isMobilePhone(value)) return 'mobilephone';

  return 'username';
}

/**
 * @this {OfficeAuth}
 */
function installEmailOrPhonePlugin() {
  const self = this;

  self.onPrependData(function(formData, $el) {
    const formType = formData.find(item => item?.name === 'action')?.value ?? 'auth/formLogin';
    const userName = formData.find(item => item?.name === 'username');
    const password = formData.find(item => item?.name === 'password');
    const phoneCode = formData.find(item => item?.name === 'phone_code');

    if (userName) {
      switch (formType) {
        case 'emailorphone/formReset':
        case 'emailorphone/formRegister':
          userName.name = _checkType(userName.value);
          break;
        case 'emailorphone/formLogin':
          userName.name = _checkType(userName.value);

          if (phoneCode?.value?.trim()?.length) {
            password.value = '';
            password.required = false;
          }
          break;
      }
    }
  });
}

export const EmailOrPhonePlugin = {
  create() {
    return installEmailOrPhonePlugin.call(this);
  }
};

export default EmailOrPhonePlugin;
