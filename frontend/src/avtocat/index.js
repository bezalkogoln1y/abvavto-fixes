import Alpine from 'alpinejs';

import '../scss/avtocat.scss';

class Api {
  static ALLOWED_METHODS = ['get'];

  _baseUrl = '';

  constructor() {
    Api.ALLOWED_METHODS.forEach(method => {
      this[method] = this._closure.bind(this, method);
    });
  }

  get(url, data) {
  }

  get baseUrl() {
    return this._formatUrl(this._baseUrl);
  }

  set baseUrl(url) {
    this._baseUrl = this._formatUrl(url);
  }

  _formatUrl(url) {
    if (url?.trim()?.length) {
      const last = url.slice(-1);
      const first = url.slice(1);

      if (last !== '/') {
        url += '/';
      }

      if (first !== '/') {
        url = '/' + url;
      }

      return url;
    }

    return '';
  }

  _closure(method, url, data) {
    if (window && window.$ && window.$[method]) {
      return new Promise((resolve, reject) => {
        window.$[method](this._baseUrl + url, data)
          .done((data, type) => {
            resolve(data);
          })
          .fail(error => {
            reject(error);
          });
      });
    }
  }
}

$(() => {
  const root = $('#ac-search');
  const api = new Api();

  api.baseUrl = 'avtocat/acat_api';

  root?.find('form')?.on('submit', (e) => {
    e.preventDefault();
  });

  Alpine.data('avtocatSearch', () => {
    return {
      searchString: '',
      results: [],
      loading: false,
      show: false,

      _highlight(result) {
        return result.map(item => {
          const newItem = item;

          Object.keys(newItem).forEach((key) => {
            const value = newItem[key];
            newItem[key] = value?.replace(this.searchString, `<strong>${ this.searchString }</strong>`) ?? value;
          });

          return newItem;
        });
      },

      _fetchItems(value) {
        if (value?.trim()?.length <= 3) {
          this.show = false;

          return false;
        }

        this.loading = true;
        this.show = true;

        api.get('search_result_json', { search_string: value, page: 1 })
          .then((data) => {
            this.results = this._highlight(data?.result || []);
            this.loading = false;
          })
          .catch(console.error);
      },

      get linkWithSearchString() {
        return '/avtocat/search?search_string=' + this.searchString;
      },

      init() {
        this.$watch('searchString', this._fetchItems.bind(this));
      }
    };
  });

  Alpine.start();
});
